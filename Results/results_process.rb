#! /usr/bin/env ruby

def stats_for_file(file)
  stats = Hash.new do |hash, key|
    hash[key] = Hash.new do |hash, key|
      hash[key] = Hash.new do |hash, key|
        hash[key] = Array.new
      end
    end
  end
  
  File.open(file, "r").each do |line|
    sample, value = line.strip.split " = "
    sample = sample.split ','
    sample[0].tr! "0-9", ""
    stats[sample[1]][sample[0]][sample[2]] << value
  end
  
  stats
end

if ARGV.length > 1
  files = ARGV
else
  files = ["evaluation_tomato.txt"]
end
sep = ", "
allstats = Hash.new

files.each do |file|
  stats = stats_for_file file
  stats.each do |key, value|
    if allstats[key]
      allstats[key].merge! value
    else
      allstats[key] = value
    end
  end
end

allstats.each do |metric, stats|
  puts metric
  stats.each do |type, stats|
    failed = 0
    nearmiss = 0
    stats.each do |neighbours, stats|
      failed += stats.count "failure"
      nearmiss += stats.count "nearmiss"
    end
    total = (failed + nearmiss)
    if total > 0
      ratio = nearmiss.to_f * 100 / total
    else
      ratio = "N/A"
    end
    puts "#{type}, #{ratio}"
  end
end

# headers_printed = false
# allstats.each do |type,stats|
#   headers = [""]
#   line = [type]
#   stats.each do |metric,stats|
#     stats.each do |neighbours,samples|
#       type = "#{metric}-#{neighbours}"
#       headers << type
#       correct = samples.count("correct").to_f / samples.count
#       nearmiss = samples.count("nearmiss").to_f / samples.count
#       line << "#{correct}(#{nearmiss})"
#     end
#   end
#   puts(headers.join(sep)) if not headers_printed
#   headers_printed = true
#   puts line.join sep
# end
