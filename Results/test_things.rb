#! /usr/bin/env ruby
require 'yaml/store'

database = Hash.new do |hash,key|
  hash[key] = Hash.new
end

Dir.glob('Images/*/*/').each do |path|
  base, name = File.split(path)
  type = /([a-z]*)([0-9]*)$/.match(name)[1]
  database[type][name] = [path]
end

database.each do |type,hash|
  next if not (ARGV.include? type or ARGV.include? "all")
  dbname = "testdb_#{type}.yaml"
  
  hash.each do |name,path|   # For each subtype in the database..
    
    # Store everything else in the database
    dbfile = YAML::Store.new dbname
    dbfile.transaction do
      database.each do |t,h|
        if t == type
          newhash = h.dup
          newhash.delete(name)
          dbfile[t] = newhash
        else
          dbfile[t] = h
        end
      end
    end
    # run the test here
    paths = Dir.glob("#{path[0]}*.png").join('" "')
    
    ["dp", "dbc", "dg"].each do |metric|
      [1,2,3,4,5,7].each do |neighbours|
        res = `src/turf.py -v -d #{dbname} -n #{neighbours} -m #{metric} "#{paths}"`
        
        if /Object looks like a (\w*)/.match(res)[1] == type
          classification = "correct"
        elsif /neighbourhood \['(.*)'\]/.match(res)[1].split("', '").count(type) > 0
          classification = "nearmiss"
        else
          classification = "failure"
        end
        
        puts "#{name},#{metric},#{neighbours} = #{classification}"
        
      end
    end
  end
end
