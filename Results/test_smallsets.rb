#! /usr/bin/env ruby

chosen_ones = ['apple10', 'car14', 'cow10', 'cup10', 'dog10', 'horse10', 'pear10', 'tomato10']
dbname = "Images/database.yaml"

chosen_ones.each do |object|
  
  imgs = Dir.glob("Images/**/#{object}/*.png").sample 5
  imgs = imgs.join " "
  
  type = object.tr "0-9", ''
  
  ["dp", "dbc", "dg"].each do |metric|
    
    res = `src/turf.py -v -d #{dbname} -n 7 -m #{metric} #{imgs}`
    
    if /Object looks like a (\w*)/.match(res)[1] == type
      classification = "correct"
    elsif /neighbourhood \['(.*)'\]/.match(res)[1].split("', '").count(type) > 0
      classification = "nearmiss"
    else
      classification = "failure"
    end
    
    puts "#{type},#{metric} = #{classification}"
  end
end