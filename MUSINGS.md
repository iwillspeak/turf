Papers
======

 * [ECCV][eccvpaper]
 * [Wikipedia][wiki]
 * [ICCV Tutorial][iccvtut]
 * Grassmann Discriminant Analysis: a Unifying View on Subspace-Based Learning

Implementation
==============

 * Calculate the PCA of each image
 * Construct a Grassmannian Matrix from this
   * This represents the manifold
   * Each item in the manifold is a vector
   * Matrix of vectors, represent in Python as a list of lists (probs)
 * Calculate the distance between GMs using Principle Angles
 * Create classifiers to sort different object types by the distances from the
   test object.

[eccvpaper]: http://www.cs.colostate.edu/~lui/Papers/ECCV08lui.pdf
[wiki]: http://en.wikipedia.org/wiki/Grassmannian
[iccvtut]: http://www.cs.brown.edu/~ls/iccv2011tutorial.html#materials