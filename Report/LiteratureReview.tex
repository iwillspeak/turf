%!TEX root = /Users/will/Dropbox/Courses/PR3/Turf/Report/Object Recognition with Grassmannian Manifolds.tex

\chapter{Literature Review} % (fold)
\label{cha:literature_review}
Object recognition is one of the great areas of study in computer science.
There is still no complete implementation for what, to a human at least, seems
a simple challenge: looking at things and describing what they represent.
Research has split into several areas, including object location, object
tracking, and facial recognition. In this chapter I present some current
techniques for facial recognition and discuss their potential application to
the more general realm of object recognition.

To perform the task of vision there are several steps that a computer must
take. Firstly it is presented with a continuously varying scene in which there
are an arbitrary number of individual objects, such as a book or a cup, each
of which can be subdivided into smaller logical objects. Take for example a
tree which has branches, each of which may have twigs and leaves and so on.
Before any objects can be recognised this scene must be sampled and a discrete
representation of it formed. From this representation regions that may form a
logical object must be identified and then each of these objects must be
classified.

\section{Image Representation} % (fold)
\label{sec:image_representation}
The initial task sampling the scene can be performed by any of a variety of
imaging devices, for example digital cameras, infra-red image sensors and
x-ray machines. These produce data as a matrix of pixel values representing
the colour or luminance of the scene at a discrete point. Each element of
the matrix representing the pixel value at the element's x and y coordinate
within the image. This representation is an intuitive one, however it does
not allow the direct creation of a manifold \citep{lui2008}. For this we
require a the image to be part of a vector space, and a new representation
of the image that has this property. An image can be thought of as a long
vector consisting of the values of each element of the rows of the matrix in
a given order, as shown in \autoref{fig:image_repr}. We will assume that all
images are represented in this form for the remaining of the report.

% This is our picture of a matrix and its long vector form. We read the 
% top row from left to right and then the next and so on.
\begin{figure}[tbh]
	\[
		I_{m,n} = \\
		\begin{pmatrix}
		i_{1,1} & i_{1,2} & \cdots & i_{1,n} \\
		i_{2,1} & i_{2,2} & \cdots & i_{2,n} \\
		\vdots  & \vdots  & \ddots & \vdots  \\
		i_{m,1} & i_{m,2} & \cdots & i_{m,n} \\
		\end{pmatrix}
		= \\
		\begin{pmatrix}
			i_{1,1} \\
			i_{1,2} \\
			\vdots  \\
			i_{m,n-1} \\
			i_{m,n} \\
		\end{pmatrix}
	\]
	\caption[Representing image matrixes]{Representing image matrixes as long vectors}
	\label{fig:image_repr}
\end{figure}

Although this image provides us with a representation of the scene it may also
contain other spurious data, such as camera noise and image artefacts. These
can arise from a variety of sources in the capture process. The quantum
behaviour of photons means they possess an inherent noise, electrical noise in
the components responsible for registering these photons, and general dust and
dirt can all contribute some degree of noise to the final image. The effect of
this noise on the recognition process can be reduced by applying one of a
variety of filters to the image such as a Gaussian blur or a non-linear median
filter. Each of these filters is good at removing certain types of noise from
the image. All noise reduction filters however will in some way degrade the
image in order to remove the unwanted data. A good all-round noise reduction
filter that generally provides minimal image degradation is the non-linear
median filter. This filter reduces the high-frequency noise whilst still
maintaining image detail \citep[p. 188]{parker_vision}. This would not remove
any artefacts caused by the camera and lens combination. It would be a
reasonable assumption to make however that if the training and test data sets
are captured with the same or similar cameras then there will be a close
correlation between these artefacts in the images and they could be ignored.


Once the image has been processed into a state where recognition can begin
the first step is to identify the locations within the image frame that
represent the logical objects within the scene. A method commonly used to
detect objects in still images is \emph{thresholding} such as the 
iterative process used by \citet{ridler78}. In this process a cut-off value
is chosen between the minimum and maximum luminance values of the image and
an attempt is made to optimise the cut-off value such that all the pixels
that belong to a single logical object in the scene lie on the opposite side
of the cut-off value than the majority of the rest of the scene. The process
of choosing an optimal value for the cut-off is, in general, a complex one. 
Although the process of thresholding can be applied to object location in
video frames more specialised techniques exist such as frame-by-frame object
tracking as described by \citet{5674053} and \citet{698614}. In this 
technique object's locations, approximate velocities and other
characteristic data from previous frames is used to estimate their new
location. This greatly reduces the amount of effort required to locate
objects within the frame as once they are known about they are no longer
searched for in the whole image but instead in a constrained area of the
image in which they are believed to most likely be. This technique still
requires a method to initially identify objects within the image. This is
often achieved by identifying areas of the image that have changed over the
past few frames. These often represent objects of interest such as people,
cars and items that people are interacting with. In some senses the extra data
provided by a sequence of video frames makes the task of object location and
tracking simpler.

% section image_representation (end)
\section{Object Recognition} % (fold)
\label{sec:object_recognition}
Once part of an image has been identified as possibly containing an object
of interest, such as a car or face, a variety of techniques can be used to
classify it. \citet{790410} proposes a method based upon identifying the 
image features that make up each different type of object, mimicking the 
task carried out by the human vision system. By relying on a large number of
features in the image for recognition \citet{790410} allows for robust
recognition even when large amounts of the object are occluded within the
scene. Many other techniques rely upon feature extraction and comparison of
the resulting graphs to perform recognition \citep{5596015,1443105,6038059}.
The approach described by \citet{5596015} uses subgraphs as the features
that make up the nodes within the graph-representation of the image. This
allows for robust recognition of features and is a novel way of improving
recognition rates. Features such as wheels or handlebars that are shared 
amongst different types of vehicles are recognised individually and then the
type of object can be inferred from the graph representing their relative\
position and scale. A similar process is described by \citet{1443105}, in
this approach pairs of features are classified based upon their relation to
each other and the type of image feature that is represented. This 
information is then translated into a vector space and classified using
\emph{nearest neighbour} techniques, that is it is not the single closest
value that determines the classification but the most frequent of the closest
neighbours. Some of the main problems with such approaches to recognition is
that the databases of object features can grow quite rapidly \citep{1443105}
and that there is often a significant amount of computation required to
perform feature extraction. Some forms of feature extraction perform more
optimally than others, especially with respect to time complexity as is
discussed by \citet{6038059}.

The sheer size of the data required to represent the knowledge about an 
object that is required to perform recognition is often the limiting factor
in recognition systems \citep{1211358}. It is not just amount of space 
required to represent the objects that can cause a problem, when attempting
to classify an object it is often compared to each item in the knowledge
database. Both of these factors end up limiting application of object
recognition. Most implementations are only capable of recognising a given set
of objects in a carefully constrained environment. A robust, reliable and
reusable method of object recognition must be able to recognise objects 
with the minimum amount of training and be capable of representing each
object in a concise manner. 

The task of facial recognition can be thought of as a special case of object
recognition. It is however an area that has received much interest; there exist
many specialised algorithms optimised for facial recognition
\citep{223162,139758}. The process described by \citet{223162} extracts feature
information from the facial images and represents each face as a vector of
these features. This approach is the basis of much commercial face detection
software, however requires that the extraction of features is carefully
tailored to optimise recognition. With such a system slight variations in the
recognition environment might mean that the recognition model needs to be
re-tuned, possibly a costly operation.

% section object_recognition (end)
\section{Grassmannian Manifolds} % (fold)
\label{sec:grassmannian_manifolds}

Recently there have been several attempts to use matrix manifolds as a means
of facial recognition \citep{Lui2012380}. This approach attempts to overcome
the problems suffered by graph-based facial recognition by removing the need
for feature detection. Sets of faces are represented directly as points on a
manifold in a higher dimensional space. In this approach the entire face is
effectively treated as one single feature. The statistical representation of
also effectively reduces the dimensionality of the data; \citet{1211358}
show that machine learning techniques can be used to reduce the size of this
representation further. By using a set of images of an object to define a
subspace the unique information that characterises each object is extracted.
One downside of this approach is its susceptibility to changes in lighting
conditions. The problems of poor or inconsistent lighting can be overcome by
approximating a lighting model and applying the inverse to remove the
lighting effects \citep{Zhang2009251}. This approach is capable of producing
consistently lit representations of an object to a high degree of 
reliability, boosting recognition rates to over 99\% even in poorly illuminated
groups of images \citep{Zhang2009251}.

The first real use of \gms\ for facial recognition was in 2004 by
\citet{1211358}. Their paper showed that by representing an image $I$ as $n
\times\ 1$ vector in an $\mathbb{R}^n$ dimensional space a matrix $U$ could be
found to translate the image from $\mathbb{R}^n$ to a lower dimensional space
such that all the information required to recognise the object was contained in
the new vector $U^{T}I$. These vectors of image characteristics are then used
to construct a manifold and the similarities between two sets of images are
calculated by finding the distance between the two points on the manifold. The
approach used generates impressive recognition rates, however it does not take
into account the nature of the manifold when calculating the distance between
two images. The paper makes the simplifying assumption that the distance
between two images is directly proportional to the difference between their
positions in the lower dimensional space \citep{1211358}.

The success of this work has made \gms\ popular as a method of characterising
images for object recognition. Both \citet{5995564} and \citet{Wang20091161}
discuss ways to improve accuracy and recognition ratios by improving the
measure of distance across the manifold. \citet{5995564} proposes calculating
a transformation matrix such that separation between the subspaces is
maximised and the recognition rate improved. \citet{Wang20091161} suggest a
kernel function that maps the input to deal with the non-linear nature of most
input data and make the process more robust with respect to pose change.

\gms\ are not the only structure that is capable of representing objects as whole rather than requiring individual features. One such approach is that of Eigenfaces \citep{139758}. In this approach face images are stacked to create an average face image. This image is then projected along it's eigenvectors using principle component analysis. A similar approach using Fisher Linear Discriminant analysis, known as Fisherfaces \citep{swets1996using}, has been shown to outperform the Eigenface approach \citep{yang2003combined}. Both of these approaches are very similar in their structure to the \gm\ methods outlined in this report, using principle component analysis and liner discriminant analysis to represent the variance of a set of images and then comparing the resulting manifold representations based upon subspace distances.

% section grassmannian_manifolds (end)
\section{Manifold Distance} % (fold)
\label{sec:manifold_distance}
The accuracy and effectiveness of matrix manifolds for image recognition is
dependent upon the effectiveness of the measure of similarity. This measure
can be degraded due to improper assumptions about how the manifold represents
the objects and inaccuracies in measuring the distance that two points are
apart on the surface of the manifold. Measures of image similarity, and thus
image recognition, can be improved by more accurately representing the
underlying structure of the data and by maximising the separation between
points on the manifold to offset any error in the distance calculation.

The nature of the underlying data that makes up the image sets for each object
is often non-linear contrary to the initial assumptions. This can be caused by
either poor or inconsistent illumination, pose change or variety in the input
images. The effect of this non-linearity can be taken into account by using the
\emph{kernel trick} to map the images into the manifold's space
\citep{Wang20091161}. This method creates a non linear mapping of the images
into the higher dimensional space that better preserves the underlying
distribution. By capturing the variation in the underlying images the manifold
distance calculations more accurately represents the relationship objects in
the underlying feature space. This allows for a wider variety of poses and
illuminations to be recognised \citep{Wang20091161}.

Increasing the separation between items in the manifold can also increase the
recognition rate. In this approach a projection kernel is created that
attempts to map the sets onto the \gm\ as uniformly as possible to increase
discriminatory power. The projection kernel is repeatedly improved using
statistical learning techniques to penalise members of a set that are spaced
apart on the surface of the manifold and reward image sets that are further
away from others \citep{5995564}. This approach has a certain computational
complexity attached to it, in the order of $O(N^{3})$, but the theory could be
used to improve the discriminatory power of any graph embedding approach. In
their paper \citet{5995564} discuss their approach in detail and provide an
in-depth mathematical discussion of how to apply it to image recognition with
\gms{}. This analysis shows that, although there is a higher computational
cost associated with this method, a smaller number of images may be needed to
form a set capable of providing reliable recognition rates. This arises from
the increase in discriminatory power provided by the projection kernel
\citep{5995564}. This seems to point towards a trade off between the amount of
images that are acquired and the time spent transforming them into a
Grassmannian representation.

% section manifold_distance (end)

\section{Conclusions} % (fold)
\label{sec:conclusions}
In their paper \citet{Wang20091161} provide a large amount of background to
the problem of using \gms\ in computer vision. The paper focuses mainly on the
mathematics and application of their proposed method. In contrast to
\citet{5995564} \citet{Wang20091161} provide a comparatively small amount of
in-depth explanation of applying the approach outlined in the paper. Both
papers tackle the idea of improving recognition rates by better measuring the
the distances between the objects in the manifold space, although they
approach it from different directions. The method outlined in \citet{5995564}
does not deal with providing a better representation of the data itself but
instead proposes a method of increasing the separation of points on the surface
of the manifold and therefore maximising the resolution possible with the 
same assumptions about the input data. This approach has the advantage that 
it can be applied to any manifold representation of image sets to maximise 
their rate of recognition. It does however require that the optimal mapping
be calculated, this could prove to be computationally expensive and is, in
general, an NP type problem. Both approaches produce impressive results and
show that there is plenty of room for further research and optimisation in 
this area.

% section conclusions (end)
% chapter literature_review (end)
