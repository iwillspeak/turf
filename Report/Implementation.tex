%!TEX root = ./Object Recognition with Grassmannian Manifolds.tex

\chapter{Implementation} % (fold)
\label{cha:implementation}

In this project I will be implementing an object recognition algorithm that utilises a \gm{} representation of objects to capture the statistically significant portions of images. The algorithm implementation takes the form of a command line Python application which loads training data configuration information from a text-based configuration file and prints the classification of the test images to standard output.

The implementation phase can be split into four main logical parts:

\begin{itemize}
	\item Loading and processing images
	\item Representing images as \gms{}.
	\item Collecting manifolds together into into the training database.
	\item Implementing classifiers.
\end{itemize}

\section{Loading Images} % (fold)
\label{sec:loading_images}

Images are loaded from disk using the Python Imaging Library (PIL) \citep{pil2013}. The PIL representation of the image is suitable for performing image manipulation tasks and displaying the image within a graphical interface. Internally the image is represented as a matrix of pixel values. PIL exposes this internal representation as a multidimensional array capable of being loaded by the Numpy library. Numpy \citep{numpy2013} provides a set of numerical types for representing various mathematical collections; such as arrays, multi-dimensional arrays and matrices. In Numpy multidimensional arrays are structurally equivalent to matrices, they only differ in the way that some mathematical operations are applied to their elements. The image representation available as an output from PIL can therefore be considered a matrix representation, as discussed in \autoref{sec:image_representation}.

Before a manifold can be created images are grouped together into an \emph{ImageSet} object. This object is then responsible for performing the transformation of the image data from the matrix format which is received form PIL into the long vector format from which the manifold is constructed.

Once the images are loaded into memory by PIL and grouped into an \emph{ImageSet} a small number of image manipulations are carried out to normalise the contents of the images. Images are rescaled, the contrast ratio of the images is corrected and each image is converted to a grayscale, or luminance, representation. This should remove any effects of colour and lighting variation from images before they are analysed to create a manifold; improving the reliability of the algorithm and making it more robust to lighting variations within the test images.

Each image is resized to a predetermined shape and size, a 32x32 pixel square. This resizing operation ensures that the object in question is centred within the image data and that the size of the long vector representation of each image is consistent. The relatively low resolution chosen provides several benefits in performing object recognition. Firstly with a smaller amount of data than that which a larger resolution image might produce the speed of the algorithm is improved. Secondly the bilinear filtering used when rescaling the image acts as a form of Gaussian filter, smoothing the contents of the image and removing high-frequency detail. After experimentation I found that resizing the image to 32x32 pixels provided the best trade of between image size and the speed of the algorithm, see \autoref{fig:image_size_speed}.

\begin{figure}[tbh]
	\centering
	\begin{tabular}{c | r r r | r}
		Image Size (px) & Run 1 (s) & Run 2 (s) & Run 3 (s) & Average (s) \\
		\hline
		16 $\times$ 16 & 4.011 & 4.010 & 4.027 & 4.016                      \\
		24 $\times$ 24 & 4.922 & 4.890 & 4.888 & 4.900                      \\
		32 $\times$ 32 & 7.075 & 7.124 & 7.165 & 7.121                      \\
		48 $\times$ 48 & 25.814 & 25.876 & 25.942 & 25.877                  \\
		64 $\times$ 64 & 109.344 & 106.964 & 109.375 & 108.561              \\
	\end{tabular}
	\caption{Run-times increase exponentially with image size}
	\label{fig:image_size_speed}
\end{figure}

The normalised image data is then transformed into a set of image vectors. Each image matrix is represented as a multi-dimensional Numpy array when received from PIL. Numpy arrays are represented as a block of values and a separate shape value, this allows arrays to be resized without moving the data around in memory which could prove a costly operation. The normalised image data is reshaped in this manner, converting the images into a set of arrays ready to be transformed into a \gm{}.

% section loading_images (end)

\section{Creating Manifolds} % (fold)
\label{sec:creating_manifolds}

Each \gm{} is constructed from the long vector representation of each image within an an image set. A matrix is constructed whose rows are long vector representation of each image, and whose columns represent a sample of the same pixel in each image within the image set. This matrix is then analysed statistically and the most statistically significant parts extracted. Principle component analysis is used to extract the main axis of variation from the matrix. The Python Modular toolkit for Data Processing (MDP) \citep{mdp2013} is used to perform PCA. An MDP \emph{PCANode} object is created and trained with the matrix containing the data from the image set. Once trained a matrix containing the variation of the images along the principle components is extracted. This matrix represents a \emph{span} of a \gm{} \citep{hamm2008grassmann} and forms the internal representation of \gms{}.

The statistical representation of the image set provided by the \gm\ representation reduces the dimensionality of the data. This means that the database of training data can be expressed in a more concise way. To speed up the loading of the training database PCA is performed lazily and cached to ensure that this costly operation is only performed the minimum number of times required to recognise the test object.

The process of extracting the significant variations within the set of images is a conceptually simple one. The $n$ main axis on which the data varies are found and the variation of the images along each axis forms the $n$ columns of the final matrix. A na\"{\i}ve method of computing the PCA of a matrix would be to repeatedly search for the direction of greatest variation within the data. Each time a direction, or basis vector, is chosen the search space for the next basis vector is reduced to those which are perpendicular to the previous basis vectors \citep{shlens2005tutorial}. In this manner the most principle basis vector would be chosen first as it represents the greatest variation within the data, creating a natural ordering of the principle component vectors.

This method of finding the principle component vectors is inefficient as there is no bound placed on the search for each component vector. A more optimal method of computing the matrix of principle components is via Singular Value Decomposition (SVD). In SVD the variance of the input data along the eigenvectors of the cross-correlation matrix is computed \citep{shlens2005tutorial}. The matrix representing these variances forms the matrix of the principle components of the the input. This method speeds up the computation required to perform PCA and is the method used by MDP to compute the PCA for the manifold.

Given a matrix $M$ the singular value decomposition of $M$ is given by $USV$ such that the following holds:
\begin{equation}
	M = U S V
\end{equation}
Where $U$ and $V$ are unary matrixes and $S$ is a rectangular matrix with the singular values along its leading diagonal \citep{shlens2005tutorial}.

% section creating_manifolds (end)

\section{Associative Containers} % (fold)
\label{sec:associative_containers}

Object classification depends upon a large database of training data to be accurate. The training data consists of sets of images of objects from each class of item to be recognised. Different training data allows recognition of a different set of objects. Training data is stored on disk as a collection of images and a YAML\footnote{YAML Aint Markup Language, a simple markup language.} configuration file. Classes of object are represented as keys in a YAML mapping. Classes can contain either mappings defining subclasses or lists of images. When loaded a manifold representing all the images within each class is available. An example configuration file is shown in \autoref{fig:yaml_config}, this configuration defines a training data set containing four objects representing two main classes of object: apples and cars. This simple text-based configuration system allows the application to be used with a variety of different image databases and allows database configuration files to be edited either by hand or by automated scripts.

\begin{figure}[tbh]
	\centering
	\begin{lstlisting}[name=example_database.yaml,style=customyaml]
apple:
    apple1:
    - image_folder/apple1
    apple2:
    - image_folder/apple2
car:
    car1:
    - image_folder/car1
    car2:
    - image_folder/car2
	\end{lstlisting}
	\caption{An example YAML configuration file}
	\label{fig:yaml_config}
\end{figure}

The configuration file is parsed and loaded by the \emph{ImageDatabase} object. This object is responsible for loading the images from disk and creating the hierarchy of \emph{ImageSet} and \emph{ImageDatabase} objects. The \emph{ImageDatabase} object is a specialisation of the Python default mapping type; the dictionary, or \emph{dict}. Inheriting from the dictionary allows the ItemDatabase to support all of the indexing and slicing operations of its parent class. The class is specialised to support several initialisers, including loading from a YAML file.

Once the image data is loaded into an ImageDatabse an \emph{ObjectDatabase} object is created to represent the contents as manifolds. The \emph{ObjectDatabase} class provides a similar representation of the training data to the \emph{ImageDatabase}, a mapping of object type strings to values. In this case each value is a \emph{Grassmann} object providing a manifold representation of each sample object. In addition to the usual dictionary indexing operations the \emph{ObjectDatabase} supports a custom iterator. This iterator is responsible for providing a sequence of pairs consisting of a \emph{Grassmann} object and a Python string representing the class to which the manifold belongs. This allows a test manifold to be compared against each item within the \emph{ObjectDatabase} when classifying. Providing the object's type string from the keys of the database allows an object to belong to more than one mapping, which would not be possible if the type was stored in the \emph{Grassmann} object itself. Using the multi-level nature of the database it would be possible after identifying an object as a car to find which car it most closely represented. This would be achieved by iterating over the contents of the \emph{car:} key. This flexibility allows both corse and fine grained recognition from the same database of objects.

% section associative_containers (end)

\section{Classifiers} % (fold)
\label{sec:classifiers}

Classification of objects is performed by a \emph{Classifier}. Each \emph{Classifier} object is responsible for finding the closest match to a test manifold from an \emph{ObjectDatabase}. Each classifier is initialised with an \emph{ObjectDatabase} representing its training data. The classifier can then be executed passing the test manifold as a parameter. The \emph{Classifier} execute method is responsible for calculating the distance between the test manifold and each element within the training data and returning the classification.

When executed the \emph{Classifier} object creates a fitness function that returns the distance between the test manifold and a second manifold. This fitness function uses one of the distance metrics implemented by the Grssmann Object. This metric is responsible for calculating an approximation of the subspace distance between the two manifolds. The fitness function is then used to order each element in the \emph{ObjectDatabase} representing the training data by its distance from the test manifold. Once ordered the nearest-neighbour heuristic is used to choose the class of the test manifold (see \autoref{sec:classification}).

The nearest neighbour heuristic can be configured to take a varied number of neighbours into account. More specifically the technique is known as the k-nearest neighbour heuristic, where k is the number of neighbours that are considered when choosing a class for the object. The accuracy of this metric is dependant on the number of neighbours chosen, and the accuracy of the distance metric.

% section classifiers (end)

\section{Subspace Distances} % (fold)
\label{sec:subspace_distances}

Distance metrics are responsible for providing a measure of the subspace distance between two manifolds. These distance metrics should conform to several invariants. For each distance metric $d$ the following should hold \cite{hamm2008grassmann}:

\begin{enumerate}
	\item The Distance between two manifold objects should equal zero if both objects represent the same manifold. That is $d(A,B) = 0$ \emph{if} $A = B$.
	\item The distance between two disjoint manifolds should be greater than zero. That is $d(A,B) > 0$ \emph{if} $A \neq B$.
	\item The distance between two manifolds should be commutative. That is $d(A,B) = d(B,A)$ holds.
	\item The distance Between two manifolds is greater via a third manifold, that is $d(A,C) >= d(A,B) + d(B,C)$ holds.
\end{enumerate}

A metric that conforms to all of these properties provides some approximation of the subspace distance between two manifolds. By ensuring that each distance metric adheres to all of these invariants we can use classifiers that rely on the distance metric approximating the subspace distance, such as the nearest neighbour classifier.

Each distance metric is dependant upon the principle angles between the two manifolds. The principle angles between the manifolds are defined as the angles between the axis of variance of each of the manifolds, ordered from smallest to largest.

The principle angles between two manifolds can be calculated by a brute-force calculation that searches each pair of unit vectors in the respective subspaces. This approach is a computationally intensive one and therefore quite time consuming. The cosines of the principle angles can be found relatively easily however. It can be shown \citep{hamm2008grassmann} that the a matrix containing the cosines of the principle angles, also known as the \emph{canonical correlations}, is produced from the singular value decomposition of the matrix product of transpose of the first span with the second span.

\begin{equation}
	Y_{1}^{'}Y_{2} = U(cos\theta)V	
\end{equation}

Where $cos\theta$ represents the diagonal matrix containing the cosines of the principle angles. As each matrix contained within a Grassmann object is a orthogonal matrix \citep{hamm2008grassmann} it is true that $M^{'} = M^{T}$ (from the definition of orthogonality of a matrix). Combining this with the above, and given that $M_1$ and $M_2$ are the matrix representations of two Grassmann objects, we can calculate the canonical correlations between two manifolds as follows.

\begin{equation}
	M_{1}^{T}M_{2} = U(cos\theta)V
\end{equation}

With $U$ and $V$ representing scalings of these cosines that can be ignored for our calculations.

% section subspace_distances (end)

\section{Geodesic Distance} % (fold)
\label{sec:geodesic_distance}

Once the canonical correlations have been found it is possible to calculate an approximation for the actual geodesic distance between the two manifolds. By assuming that all angles are less than $\pi/2$ we can use the $arccosine$ function to compute the principle angles. From these principle angles we can derive the geodesic distance. It is known \citep{wong1967differential} that the geodesic distance is related to the sum of the squares of the principle angles as follows.

\begin{equation}
	d_{G}(Y_1,Y_2) = \sqrt{\sum_{i}{\theta_{i}^{2}}}
\end{equation}

Where $d_{G}$ is the geodesic distance function, $\theta_i$ is the $i$th principle angle between $Y_1$ and $Y_2$, and $m$ is the number of principle angles. Using this an approximation of the geodesic distance between two manifolds can be computed. This distance will closely approximate the true geodesic distance for manifolds separated by small angles. For manifolds separated by angles of greater than $\pi/2$ the distance will be an under-approximation. This inaccuracy stems from the limitations of the $arccosine$ function. To attempt to overcome this problem the Projectction and Binet-Cauchy metric depend upon cosines of the principle angles, rather than the angles themselves.

% section geodesic_distance (end)

\section{Projection Metric} % (fold)
\label{sec:projection_metric}

The \emph{Projection Metric} overcomes the Geodesic metric's limitations with regard to the angle between the subspaces. The projection metric is defined as the Euclidean norm (2-norm) of the sine of the principle angles \cite{hamm2008grassmann}.

\begin{equation}
	d_{P}(Y_1,Y_2) = \sqrt{\sum_{i}{sin^{2}\theta_{i}}}
\end{equation}

By utilising the equality $sin^{2} \theta = 1 - cos^{2} \theta$ we can express this metric in terms of the $cosine$ function.

\begin{equation}
	d_{P}(Y_1,Y_2) = \sqrt{\sum_{i\; =\; 0}^{m}{1 - cos^{2}\theta_{i}}}
	= \sqrt{m - \sum_{i\; =\; 0}^{m}{cos^{2}\theta_{i}}}
\end{equation}

This form allows the metric to be easily calculated from the canonical correlations and has a range of between $0$ and $\sqrt{m}$.

% section projection_metric (end)

\section{Binet-Cauchy Metric} % (fold)
\label{sec:binet_cauchy}

The Binet-Cauchy metric shares many of the properties of the Projection metric. The metric itself can easily be calculated from the canonical correlations. The metric is calculated from the product of the canonical correlations \citep{hamm2008grassmann} and has a range of between $0$ and $1$.

\begin{equation}
	d_{BC}(Y_1,Y_2) = \sqrt{1 - \prod_{i}{cos^{2}\theta_{i}}}
\end{equation}

Both the Binet-Cauchy and Projection metrics, unlike other metrics such as the \emph{Maximum Correlation} metric, utilise all of the principle angles and produce a distance metric that is likely to be further from zero in the case of closely grouped subspaces \citep{hamm2008grassmann}. This property makes them especially useful when the limitations of floating point numbers, such as rounding error and precision error may be encountered.

% section binet_cauchy (end)

\section{Summary of Metrics} % (fold)
\label{sec:summary_of_metrics}

Although there are theoretical limitations with the \emph{Geodesic} metric, both the \emph{Binet-Cauchy} and \emph{Projection} metrics satisfy all of the constraints outlined in \autoref{sec:distance_metrics}. The Geodesic metric satisfies all the conditions when supplied with the true principle components, when the components are computed with the $arccosine$ function there is the possibility that the fourth constraint might not hold and that inconsistent distances could be returned. For manifolds that are fairly closely grouped however this should not be an issue.

In the implementation of the Grassmann object the Geodesic, Binet-Cauchy and Projection distance metrics $d_G$, $d_{BC}$ and $d_P$ are implemented by the \texttt{dg}, \texttt{dbc} and \texttt{dp} methods respectively.

Each of the metrics is related to the Euclidean norm and uses the \emph{square} and \emph{square root} functions. The main property that this gives each metric is that negative distances are normalised so that the distance between two manifolds is the same regardless of the order in which they are compared. This is required for the metrics to satisfy the second and third constraint in \autoref{sec:distance_metrics}. It would be possible to achieve a similar result by computing the absolute value with the modulus function. This may provide better results where the precision of the floating point numbers in use in the calculations introduce errors and could reduce the computation required to calculate the distances.

% section summary_of_metrics (end)

% chapter implementation (end)
