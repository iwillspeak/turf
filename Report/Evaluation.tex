%!TEX root = ./Object Recognition with Grassmannian Manifolds.tex

\chapter{Evaluation} % (fold)
\label{cha:evaluation}

The Python programming language is highly dynamic and has a large number of pre-existing modules making it a good choice for rapidly developing an application. The ease with which Python can be interfaced with lower level languages such as C and FORTRAN means that libraries such as NumPy and MDP are both simple to use and fast. The availability of these specialist modules allows access to the specialist features of some data processing languages, such as Matlab, alongside the features of a general purpose programming language.

The use of the Test Driven Development (TDD) design methodology provided a good structure to the development process. It ensured that all of the implementation was well tested and that all code in the implementation is part of a feature. TDD is an agile development process, it focuses on process of rapidly developing features and carrying out small development tasks rather than large ones. The use of an automated test runner during the process allowed continuous testing of the codebase without interrupting the development process. Having an immediate indication of the validity of each code change ensured that errors were caught early and allowed experimentation with the implementation of each feature.

The use of a version control system during development also helped as all of the history of the codebase was available to look back at. The ability to quickly and easily roll-back changes ensured that changes could be made without worrying about the process of reversing them if design decisions proved to be a poor choice further down the line.

Overall the tools and design methodologies chosen allowed for agile development and ensured that the implementation was both simple and robust.

\section{Implementation Review} % (fold)
\label{sec:implementation_review}

The object oriented nature of the design outlined in \autoref{cha:design} abstracts the implementation details of each section away from one another. This abstraction allowed each section of of the implementation to be developed independently and tested with unit tests. The clear interfaces created by object oriented design allowed mock objects to be used in the unit tests to both allow each object to be developed in isolation and to enable the development of sections before their dependancies.

During the development the resulting speed of the recognition algorithm was an important factor in some of the design decisions made. The design of the \emph{Grassmann} object implements lazy-evaluation of the principle component analysis (PCA) to generate the matrix representing the span that the \emph{Grassmann} object covers. Once the PCA is performed it is cached within the object and the image data from which it is constructed discarded. This means that the PCA is only calculated at most one time for each Grassmann object. If the result is not used then the calculation is not done.

Removing the image data when we no longer need it reduces the size of the object within memory, allowing the application to require less memory from the operating system and making Grassmann objects quicker to serialise and de-serialise. The speed at which Grassmann objects can be serialised and de-serialised is important as a cache of the loaded ObjectDatabase is created. Reducing the time taken to load the ObejectDatabase from the cache ensures that there is as small an overhead as possible on the general use case.

\begin{figure}[tbh]
	\centering
	\begin{tabular}{ l | c c }
			& Cached & Uncached                                            \\
	\hline                                                                 \\
	Apple 	& 4.829s & 1:25.959s                                           \\
	Car 	& 4.863s & 1:19.706s                                           \\
	Cow 	& 4.847s & 1:20.107s                                           \\
	Cup 	& 4.823s & 1:19.592s                                           \\
	Dog 	& 4.902s & 1:19.430s                                           \\
	Horse 	& 4.869s & 1:19.323s                                           \\
	Pear 	& 4.951s & 1:21.492s                                           \\
	Tomato 	& 4.858s & 1:20.612s                                           \\
	\end{tabular}
	\caption{Comparison of cached and uncached recognition times}
	\label{fig:cached_uncached_speeds}
\end{figure}

The ability to quickly perform recognition is one of the requirements of the implementation. The use of lazy evaluation, SVD to compute the principle angles and caching of the training data means that, with a valid database cache, recognition can be completed in about 5 seconds. Even without a cached database recognition with the \ET{} data set is usually completed in less than 2 minutes (see \autoref{fig:cached_uncached_speeds}).

% section implementation_review (end)

\section{Image Databases} % (fold)
\label{sec:image_databases}

To test the effectiveness of the algorithm a measure of the recognition rate is needed. To properly test the recognition rate it is necessary to have a test data set. For the purpose of this report the \ET{} data set \citep{1211497} will be used. It is comprised of a series of images of a variety of objects from a large number of angles, see \autoref{fig:label}. There are 8 categories of object, each category has images of 10 instances of that object taken from 41 angles. The variety of angles and neutral background make the set an ideal basis for creating a database of \gms{}.

\begin{figure}[tbh]
	\centering
		\begin{subfigure}[b]{0.3\textwidth}
			\includegraphics[width=\textwidth]{Figures/apple4-066-153.png}
			\caption{Apple}
		\end{subfigure}
		~
		\begin{subfigure}[b]{0.3\textwidth}
			\includegraphics[width=\textwidth]{Figures/cup8-066-153.png}
			\caption{Cup}
		\end{subfigure}
		~
		\begin{subfigure}[b]{0.3\textwidth}
			\includegraphics[width=\textwidth]{Figures/horse6-066-153.png}
			\caption{Horse}
		\end{subfigure}
	\caption{Examples of objects in the \ET{} database}
	\label{fig:label}
\end{figure}

The \ET{} database has been used several times before in literature \citep{1211497,5995564,hamm2008grassmann} allowing the effectiveness of the algorithm to be compared. The set itself has a wide variety of objects: apples, cars, cows, cups, dogs, horses, pears, and tomatoes. Some of these objects closely resemble each other; such as apples and pears, horses and cows. This similarity means that some objects are more difficult to recognise than others.

One of the advantages of performing object recognition with \gms\ is that it is not that sensitive to the data used as it relies on statistical methods. The neutral backgrounds in the \ET{} set make the process more reliable, however similar effects can be achieved by segmenting the objects from the background and masking off the background pixels. This process is itself a complex task and is mainly carried out currently with facial data as a simple facial shape model can easily be generated and data outside the facial area cropped out or ignored \citep{lui2008,hamm2008grassmann}.

% section image_databases (end)

\section{Recognition Performance} % (fold)
\label{sec:recognition_performance}

In order to assess the recognition rates a series of recognition tests was performed. For each object in the \ET\ data set the recognition performance for a variety of different numbers of neighbours was assessed when the training data contained all other members of the \ET\ set. For each test the data set was split and a new configuration file created containing all other objects within the \ET\ data set. The recognition program was then run using the new configuration file and all images of the chosen element of the \ET\ set once for each of 1, 2, 3, 4, 5, and 7 neighbours for each of the three distance metrics.

For each of the classes of object a ratio of those objects of that class correctly identified to those which were not was calculated. This ratio is the recognition rate of the algorithm for that class of object. Mean recognition rates for each number of neighbours and each type of object were calculated as well as an overall mean recognition rate for each of the distance metrics. This data can be found in \autoref{fig:proj_rates} (\emph{Projection} Metric), \autoref{fig:binet_cauchy_rates} (\emph{Binnet-Cauchy} Metric), and \autoref{fig:geodesic_rates} (\emph{Geodesic} Metric).

\begin{figure}[tbh]
	\centering
	\begin{tabular}{c | r r r r r r | r}
		 & \multicolumn{6}{|c|}{Neighbours} &                                \\
		 & 1 & 2 & 3 & 4 & 5 & 7 & mean                                      \\
		\hline
		apple & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000        \\
		car & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000          \\
		cow & 0.700 & 0.700 & 0.800 & 0.800 & 0.800 & 0.800 & 0.767          \\
		cup & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900          \\
		dog & 0.700 & 0.700 & 0.700 & 0.700 & 0.700 & 0.700 & 0.700          \\
		horse & 0.800 & 0.800 & 0.800 & 0.900 & 0.900 & 0.900 & 0.850        \\
		pear & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000         \\
		tomato & 0.800 & 0.800 & 1.000 & 1.000 & 1.000 & 1.000 & 0.933       \\
		\hline
		mean & 0.863 & 0.863 & 0.900 & 0.913 & 0.913 & 0.913 & 0.894         \\
	\end{tabular}
	
	\caption{Recognition rates for the Projection metric}
	\label{fig:proj_rates}
\end{figure}

\begin{figure}[tbh]
	\centering
	\begin{tabular}{c | r r r r r r | r}
		 & \multicolumn{6}{|c|}{Neighbours} &                                \\
		 & 1 & 2 & 3 & 4 & 5 & 7 & mean                                      \\
		\hline
		apple & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000        \\
		car & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000          \\
		cow & 0.600 & 0.600 & 0.600 & 0.600 & 0.700 & 0.700 & 0.633          \\
		cup & 0.800 & 0.800 & 0.800 & 0.800 & 0.800 & 0.800 & 0.800          \\
		dog & 0.600 & 0.600 & 0.600 & 0.600 & 0.600 & 0.600 & 0.600          \\
		horse & 0.800 & 0.800 & 0.800 & 0.900 & 0.900 & 0.900 & 0.850        \\
		pear & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900         \\
		tomato & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.900       \\
		\hline
		average & 0.825 & 0.825 & 0.825 & 0.838 & 0.850 & 0.850 & 0.835      \\
	\end{tabular}
	
	\caption{Recognition rates for the Binet-Cauchy metric}
	\label{fig:binet_cauchy_rates}
\end{figure}

\begin{figure}[tbh]
	\centering
	\begin{tabular}{c | r r r r r r | r}
		 & \multicolumn{6}{|c|}{Neighbours} &                                \\
		 & 1 & 2 & 3 & 4 & 5 & 7 & mean                                      \\
		\hline
		apple & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000        \\
		car & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000          \\
		cow & 0.700 & 0.700 & 0.800 & 0.800 & 0.800 & 0.800 & 0.767          \\
		cup & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 0.800 & 0.883          \\
		dog & 0.700 & 0.700 & 0.600 & 0.600 & 0.600 & 0.600 & 0.633          \\
		horse & 0.800 & 0.800 & 0.800 & 0.900 & 0.900 & 0.900 & 0.850        \\
		pear & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000         \\
		tomato & 0.900 & 0.900 & 0.900 & 0.900 & 0.900 & 1.000 & 0.917       \\
		\hline
		average & 0.875 & 0.875 & 0.875 & 0.888 & 0.888 & 0.888 & 0.881      \\
	\end{tabular}
	
	\caption{Recognition rates for the Geodesic metric}
	\label{fig:geodesic_rates}
\end{figure}

\begin{figure}[tbh]
	\centering
	\includegraphics[width=\textwidth]{Figures/Recognition_Rates}
	\caption{Recognition rates against neighbours for each distance metric}
	\label{fig:recognition_rates_graph}
\end{figure}

By comparing the overall mean and highest individual mean recognition rates for each metric it seems that the Projection metric is the most reliable. The Projection metric has an overall mean recognition rate of $0.894$ compared to the $0.835$ for the Binet-Cauchy metric and $0.881$ for the Geodesic metric. The highest individual mean recognition rates for each of the metrics occurs for the 7-nearest neighbours set. For this set the Projection metric has a mean recognition rate of $0.913$, over $90$\%, compared to $0.850$ for the Binet-Cauchy metric and $0.888$ for the Geodesic metric. The Geodesic metric appears to be the next most strong distance metric, even performing better than the Projection metric for very low numbers of neighbours.

The data in \autoref{fig:recognition_rates_graph} shows the overall trend of recognition rates for each of the three metrics and and the six different numbers of neighbours examined. From this graph a clear trend towards a higher recognition rate for a larger number of neighbours is evident. The Projection and Geodesic metrics are also clearly higher than the Binet-Cauchy metric for all of the test cases. From this data it is unclear if the recognition rates have plateaued between five and seven neighbours or if the upward trend continues. Based upon the results from previous investigations \citep{lui2008} it is reasonable to assume that the trend continues up to a point closer to 100 neighbours.

Overall the recognition rates achieved are quite reliable. All combinations of both distance metrics and number of neighbours achieve a recognition rate of over $80$\%. With the Projection metric and more than 6 neighbours the rate of recognition misses is below $10$\%. Given the relatively low complexity of the implementation compared to some domain specific recognition approaches and the simplicity of the creation of the training data set this is an encouraging result. Existing literature \citep{lui2008, 5995564} indicates that a reasonable improvement upon the effectiveness of the recognition model can be achieved by performing a \emph{Graph Embedding}. This would most likely raise the recognition rate far closer to $100$\%.

For those cases where the object was not correctly identified we can break down the recognition failures into two main groups. Cases where the class of object was identified as one of the neighbours, but not the most common one, are classed as \emph{near misses} (see \autoref{cha:requirements}). A summary of the percentage of recognition failures that were near misses is shown in \autoref{fig:nearmiss_data}. Form this data we can see that a large proportion of recognition failures fall into the near miss category. This suggests that with a slightly larger neighbourhood the object would be correctly classified.

\begin{figure}[tbh]
	\centering
	\begin{tabular}{ c | r r r | r}
	 & \multicolumn{1}{c}{$d_p$} & \multicolumn{1}{c}{$d_{bc}$} & \multicolumn{1}{c|}{$d_g$} & mean \\
	\hline
	apple &  N/A &  N/A &  N/A & N/A                                         \\
	car &  N/A &  N/A &  N/A & N/A                                           \\
	cow & $64.286\%$ & $40.909\%$ & $50.000\%$ & $51.732\%$                  \\
	cup & $33.333\%$ & $41.667\%$ & $14.286\%$ & $29.762\%$                  \\
	dog & $44.444\%$ & $54.167\%$ & $36.364\%$ & $44.992\%$                  \\
	horse & $33.333\%$ & $44.444\%$ & $33.333\%$ & $37.037\%$                \\
	pear &  N/A & $66.667\%$ &  N/A & $66.667\%$                             \\
	tomato & $50.000\%$ & $66.667\%$ & $80.000\%$ & $65.556\%$               \\
	\end{tabular}
	\caption{Percentages of recognition failures classed as near misses}
	\label{fig:nearmiss_data}
\end{figure}

% section recognition_performance (end)

\section{Requirements Review} % (fold)
\label{sec:requirements_review}

The main requirements for the implementation are discussed in \autoref{cha:requirements}. In this section an assessment of the extent to which each requirement is met is conducted. Overall the recognition algorithm performed well, although there is still much possibility for improvement.

The first requirement was that the application be capable of reliably recognising objects. The evaluation has shown that the application is capable of identifying objects with a high degree of accuracy with all three of the distance metrics. Each of the metrics achieved an average recognition rate of over $80\%$ with the \ET{} data set. The Projection metric achieved a mean rate of recognition of over $91\%$ with 5 and 7 neighbours. It is likely that with a larger number neighbours the recognition rates would be even higher. The rates of recognition, especially with larger numbers of neighbours being considered, prove that the implementation is capable of reliable recognition.

The second requirement was that the application be capable of identifying objects correctly with as few as 5 images in the test data set. This small number of images provides quite a challenge to the approach outlined in this report as it greatly restricts the amount of data about the object that is available. With a smaller number of images of the test object the affects of noise in the image frames such as the surfaces they are standing on and background objects becomes more significant. Even with this reduced amount of information the algorithm is capable of performing reasonably. The data in \autoref{fig:small_samples_data} shows how the algorithm performed when recognising ten objects that were not in the training database based upon a random sample of five images. The projection metric again proved strong in this test. This shows that the approach is capable of recognising objects even with a greatly reduced amount of test data.

\begin{figure}[tbh]
	\centering
	\begin{tabular}{l | l l l | r}
		& \multicolumn{1}{c}{$d_p$} & \multicolumn{1}{c}{$d_{bc}$} & \multicolumn{1}{c|}{$d_g$} & mean \\
		\hline
		apple & correct & correct & correct & 1.00               \\
		car & correct & correct & correct & 1.00                 \\
		cow & correct & near miss & correct & 0.66               \\
		cup & near miss & near miss & near miss & 0.00           \\
		dog & near miss & near miss & correct & 0.33             \\
		horse & near miss & near miss & near miss & 0.00         \\
		pear & correct & correct & correct & 1.00                \\
		tomato & correct & near miss & near miss & 0.33          \\
		\hline
		       & 0.63 & 0.38 & 0.63 & 0.54                       \\
	\end{tabular}
	\caption{Recognition rates with 5 test images}
	\label{fig:small_samples_data}
\end{figure}

Requirement 3 specifies the input format that the application should expect. The application is capable of loading training data from a variety of locations, controllable via command line parameters. The \ET\ database meets the requirements to be used as a training data set. The \ET\ data set contains images from a 41 different angles on a neutral blue background \citep{1211497}.

Another requirement of the input to the program is that the training data set should be configurable. This is to enable the application to be used with a different set of training data to allow it to recognise a different set of object types. The data is configured with and loaded from a YAML file. This file contains a natural hierarchy. The training data format is both simple and flexible allowing a wide variety of image data sets to be used with the application. The ImageDatabase and ObjectDatabase classes allow for multiple levels of hierarchy within the training data. When combined with the command line parameters to select which database to load this provides a great deal of flexibility for loading training data.

The fourth requirement states that classification should be based upon a manifold distance metric. In the implementation each object is classified using information in the training set by means of one of three manifold distance metrics. Each of these metrics is capable of a high level of reliability in classification and derives distance from the principle angles between two \gms{}.

Requirement 5 was that the application is capable of recognising objects in under 10 seconds. The application makes use of lazy evaluation and caching to reduce the time spent performing computationally complex operations and increase the performance when loading the training data. The size of the internal image representation has also been chosen to improve the speed of operation (see \autoref{fig:image_size_speed}). Each of these measures speeds up the operation of the application, allowing recognition to take place quickly. With a valid training database cache available recognition is reliably performed in under 5 seconds, well within the requirement (see \autoref{fig:cached_uncached_speeds}). Performing PCA to generate the \gm\ representation of each image set representing a given object in the training data is a costly operation however. It is not really conceivable that recognition could be performed in less than 10 seconds without caching of the loaded database of \gm\ representations.

In summary the implementation was capable of meeting all of the requirements stated in \autoref{cha:requirements}. Some of the requirements present a tradeoff however. Performing recognition with a small number of images of the test object greatly reduced the effectiveness of the algorithm and fast recognition speeds are only achievable if the database of training data is pre-computed and cached.

% section requirements_review (end)

% chapter evaluation (end)
