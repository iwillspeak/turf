%!TEX root = ./Object Recognition with Grassmannian Manifolds.tex

\chapter{Design} % (fold)
\label{cha:design}

Int this section the main design considerations are discussed. This includes the choice of language, tools and development approach as well as a discussion of the design of the main areas of the recognition implementation. This includes the overall structure of the application as well as the approach taken to representing \gms{}, the choice of distance metrics to use and the details of the classification methods. The performance considerations for the implementation are also discussed.

There are a wide variety of programming languages suitable for developing image recognition applications. The three main programming environments considered for this project are \emph{C++}, \emph{Matlab} and \emph{Python}. The wide variety of third party libraries and the fast execution of the resulting application make C++ a good choice for creating image recognition algorithms for production use. As C++ is a compiled language there are slightly higher development overheads and this makes it less suitable for rapidly prototyping an implementation. Both Matlab and Python are dynamic languages with good support for numerical computation and image processing. Their dynamic nature makes them ideally suited to the requirements of this project as applications can be rapidly developed and refactored with minimal overheads.

For the implementation phase I have chosen to use the \emph{Python} programming language as I have more experience developing applications in it than in Matlab. Python is a mature dynamic language with a wide variety of third party libraries available; making it ideal for rapidly implementing algorithms. I will be using the Python Imaging Library (PIL) \citep{pil2013} and the Modular toolkit for Data Processing (MDP) \citep{mdp2013} in the implementation. All the images files used in the training database will be stored as Portable Network Graphic files (PNGs), this format can easily be loaded and manipulated with the PIL module. Large portions of these third party libraries are implemented in native code mitigating some of the performance impact of choosing a dynamic interpreted language.

The implementation will be split into two main modules. One module will be responsible for managing the database of training images and converting them into the image vector form that is required for creating \gms{}. The second module will be responsible for implementing the manifold representations and allowing comparison and classification of manifolds. These two modules will then be used by a small wrapper script to implement the functionality required. This wrapper script will be responsible for exposing a command line interface.

The training data will be stored on disk as a collection of individual images and a configuration file. This file will be used to group images together into manifolds and allow groups of manifolds that represent the same class of object to be combined. The format of this configuration file will be a YAML document forming a hierarchical mapping of strings to groups of paths. Each path can either be a single image or a directory containing images. If a directory is given all the images from that directory will be used in creating the manifold.

The YAML markup language is relatively simple compared to other text-based markup languages such as XML. The wide language support and human readable format make it simple to integrate into the application and allow easy editing of the database files by hand as well as generating them from a scripting language for testing or automation purposes. 

During the development phase I will use Test Driven Development (TDD) with the \emph{Nose} test suite and the \emph{Snooper} continuous test runner. TDD is an agile methodology which focuses on creating unit tests before implementing functionality. This approach will help to ensure that the application meets the requirements and also helps to make sure that the \emph{dead code} that does not implement any functionality is not written. As it is an agile development method it allows a great deal of freedom in the development process while still maintaining a formal structure to fall back on. 

Source code will be managed using the \emph{git} source control management system. This will allow me to keep track of development history, browse older revisions, and quickly and easily implement new features without worrying about corrupting the codebase. The distributed nature of the git system allows greater freedom in the development process as source code management is not tied to a central server. 

\section{Module Structure} % (fold)
\label{sec:module_structure}

Four main classes will be responsible for loading, representing and comparing manifolds (\autoref{fig:module_overview}). Images will be loaded into the application and stored in a vector form by the \emph{ImageSet} object. The \emph{ImageDatabase} class will provide an associative container of these images; keyed by the image's class (car, horse, etc.) as defined in the configuration file. Each group of \emph{ImageSet} objects will be associated with a \emph{Grassmann} object containing the \gm\ representation of images. These \emph{Grassmann} objects will also be stored in an associative container. The \emph{Grassmann} will support a comparison operator which will enable the database to be searched for the closest matching manifold. This operator will be implemented as one of the following distance metrics: \emph{Binnet Cauchey}, \emph{Projection}, and \emph{Geodesic}. Each of these metrics provide an approximation of the similarities of the image sets described by each manifold.

% This is a picture of the main modules: Images and Manifolds wtih the two main
% classes in each and their relation to each other.
\begin{figure}[tbh]
	\centering
	\includegraphics[width=0.9\textwidth]{Figures/ModuleOverview}
	\caption{An overview of the module structure}
	\label{fig:module_overview}
\end{figure}

% section module_structure (end)

\section{Manifold Construction} % (fold)
\label{sec:manifold_construction}

Input to the recognition algorithm takes the form of sets of images. Each image will be loaded into the application and represented initially as a two dimensional array of pixels. This two dimensional data is not suitable for translation into a manifold representation immediately and will be translated into a vector representation similar to the translation described in \autoref{fig:image_repr}. This vectorisation process maintains the ordering of the pixels within the image. There is no loss of data in this representation however it provides a more succinct representation of the data and allows for simpler representations of the algorithms to process it. With images represented in this vector format the image sets are represented as matrixes with the columns of the matrix representing the vectorisation of each image.

The matrix representation of image sets is a very high dimensional structure. The amount of data contained in each of these image sets is too large to perform any effective comparisons. The dimensionality of the data is reduced by translating each image set into a matrix representation of the span of a \gm{}. This manifold span is represented by the matrix representation of the principle components of the image set matrix. The process of translating the image set into the manifold span via principle component analysis (PCA) allows the dimensionality of the data to be reduced while still representing a large amount of the statistical variation within the data. For the \ET\ database an average of $63.12\%$ of the variance within the data is explained by reducing the data to the five most principle dimensions with PCA.

With the dimensionality of the data reduced a meaningful comparison of the image sets can be undertaken. The manifold representation of the image sets are compared by calculating the subspace distance between the manifolds they represent. This calculation is performed by analysing the angles between the manifolds on each of the dimensions. From these angles a variety of calculations can be performed to calculate an approximation of the distance. Functions that perform this calculation are referred to as \emph{Distance Metrics}.

% section manifold_construction (end)

\section{Distance Metrics} % (fold)
\label{sec:distance_metrics}

In this project I will be comparing the effectiveness of three different distance metrics. Each of these metrics provides an approximation of the distance between two manifolds. This distance is the main measure of similarity between two manifolds. A pair of manifolds with a shorter distance between them are more similar than those with a larger inter-manifold distance. There are a variety of different distance metrics suitable for use in the recognition algorithm, in this project I have chosen to implement the Binet-Cauchy, Projection and Geodesic distance metrics.

Each metric derives the distance between the manifolds based upon the \emph{principle angles} between the two manifolds. Each \gm\ matrix represents a span of the subspace represented by that manifold. For two Grassmann objects represented by the spans $Y_1$ and $Y_2$ the objects should be considered equal if both $Y_1$ and $Y_2$ span the same subspace. In this case one span represents a scaling of the other span and therefore the angels between the spans will be zero. For two spans that represent different manifolds the principle angles between the two subspaces that they span composes of the angles between all unit vectors in both subspaces. These angles are grouped in a vector ordered so that the largest, or most principle, is first. This vector of angles is the principle angles between the two subspaces.

From the principle angles a measure of the distance separating two manifold spans can be calculated. The simplest measure is the Geodesic distance metric. In this metric the actual distance between the two manifolds is calculated. Due to limitations imposed by some methods of calculating the principle component angles the result of this metric may not always be accurate. To overcome these limitations the Binet-Cauchy and Projection metrics provide a measure that is related to the geodesic distance but that can be more reliably calculated. The differences between the effectiveness of each metric is dependant upon the separation of each of the manifolds. For closely grouped manifolds the Geodesic metric should provide the most accurate measure. For less closely grouped items however the Binet-Cauchy and Projection metrics should prove to be more reliable.

% section distance_metrics (end)

\section{Classification} % (fold)
\label{sec:classification}

With an appropriate distance metric object classification consists of two main stages. First the distance between the test manifold and all the items in the training data is calculated. Once these distances have been calculated a classifier is used to choose a category to which the test manifold belongs. The classifier provides a heuristic that takes into account the subspace distances of each manifold from the test manifold. The \emph{nearest neighbour} classifier that I will be using considers the $n$ nearest manifolds to the test manifold. The class of object that has the largest number of manifolds within the $n$ neighbours is chosen as the most likely type of the test manifold. If there is no one set that is the most numerous then the set which contains the manifolds that have the smallest distance to the test manifold is chosen (\autoref{fig:nearest_neigbour}).

In addition to the nearest neighbour classifier there are other methods of measuring the similarity based upon the manifold distance. These include \emph{discriminant analysis} \citep{hamm2008grassmann} and \emph{graph embeddings} \citep{5995564}. Each of these methods provides a slightly more robust method of classification however the logic required to perform the classification is more complex. Although they would most likely prove to be slightly more robust classifiers for recognition I have chosen not to use them in this investigation as the benefits from their use should not make any major impact on the overall conclusions drawn on the suitability of \gm\ methods for general purpose object recognition.

\begin{figure}[tbh]
	\centering
	\includegraphics[width=0.9\textwidth]{Figures/NearestNeighbour}
	\caption{Example applications of the nearest neighbour classifier}
	\label{fig:nearest_neigbour}
\end{figure}

Classification will be implemented via a \emph{Classifier} object. This object will be responsible for storing references to the test manifold and the training data set. It will provide a method that classifies the test manifold and returns a string representing the class within the training data that the test object belongs to. The Classifier object should also allow inspection of the neighbours considered in the classification in order to determine near miss classification failures.

% section classification (end)

\section{Speed Considerations} % (fold)
\label{sec:speed_considerations}

Singular value decomposition is used in both the calculation of the principle component analysis when generating the manifold representation of image sets and in computing the principle angles between manifolds. Although this choice was made in order to reduce the computational complexity and speed up the application both of these computations pose performance bottlenecks for the application. In these cases SVD offers a performance benefit over more na\"{\i}ve methods however it is still a computationally complex operation to perform, requiring $O(mn^2)$ operations in the worst case. The SVD operations needed to compute the principle angles are necessary for the classification of the test object. The computation involved in generating the matrix representation of each element of training data however only needs to be performed when the database configuration changes. This makes it an ideal candidate for caching.

Once the training data is loaded a cache of the database will be written when the PCA has been performed. This cache will be written to disk when the application has finished classification. When loading the database the application will then be able to skip loading the images from disk and performing PCA for each image set if the database cache is still valid.

This cache means that if there is no change in the configuration file for the database then there is no need to re-create all the objects involved. Due to the caching of the results of the PCA this means that the construction of each Grassmann object's matrix is only done once for each change to the database configuration file. As this is likely to be an infrequent occurrence the speed of the algorithm is greatly improved in the general case.

% section speed_considerations (end)

% chapter design (end)
