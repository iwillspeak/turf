from classifier import *
from nose.tools import assert_raises
import manifold

# Mocks
class DummyGrassmann(object):
	
	def __init__(self, position):
		self.position = position
	
	def dg(self, other):
		return abs(other.position - self.position)
	
	def dp(self, other):
		return self.dg(other)
	
	def dbc(self, other):
		return self.dg(other)

class DummyObjectDatabase(manifold.ObjectDatabase):
	
	def __init__(self, mapping):
		for k,v in mapping.iteritems():
			self[k] = DummyGrassmann(v), None

# Tests
class TestClassifier(object):
	
	def test_create(self):
		
		assert Classifier(DummyObjectDatabase({"Foo": 1}))
		
		assert_raises(EmptyDatabaseError, lambda : Classifier(DummyObjectDatabase({})))
		assert_raises(TypeError, lambda : Classifier())
	
	def test_classify(self):
		c = Classifier(DummyObjectDatabase({"Bar":12}))
		assert hasattr(c, "neighbourhood")
		assert not c.neighbourhood
		assert c.execute(DummyGrassmann(12))
		assert c.neighbourhood
		assert c(DummyGrassmann(12))
		assert c.neighbourhood
		assert c(DummyGrassmann(12)) == "Bar"
		assert c.neighbourhood
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}))
		assert not c.neighbourhood
		assert c(DummyGrassmann(12)) == "B"
		assert c.neighbourhood
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), metric="dp")
		assert c(DummyGrassmann(12)) == "B"
		assert c(DummyGrassmann(143)) == "C"
		
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), metric="dbc")
		assert c(DummyGrassmann(12)) == "B"
		assert c(DummyGrassmann(143)) == "C"
		
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), metric="dg")
		assert c(DummyGrassmann(12)) == "B"
		assert c(DummyGrassmann(143)) == "C"
		
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), metric="foobar")
		assert_raises(AttributeError, lambda : c(DummyGrassmann(1338)))
	
	def test_neighbours(self):
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), 2)
		assert not c.neighbourhood
		c(DummyGrassmann(12))
		assert len(c.neighbourhood) == 2
		assert sorted(c.neighbourhood) == ["A", "B"]
		
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}), 1)
		assert not c.neighbourhood
		c(DummyGrassmann(12))
		assert len(c.neighbourhood) == 1
		assert c.neighbourhood[0] == "B"

		# Check the default length
		c = Classifier(DummyObjectDatabase({"A": 0, "B": 10, "C": 100}))
		assert not c.neighbourhood
		c(DummyGrassmann(123))
		assert len(c.neighbourhood) == 3
		assert c.neighbourhood == ["C", "B", "A"]
		assert sorted(c.neighbourhood) == ["A", "B", "C"]
