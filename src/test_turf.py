from turf import *
from nose.tools import assert_raises
import sys

class TestTurf():

	def test_main(self):
		# These tests don't check that much. it does ensure that the code
		# compiles and accepts the parameters though
		assert_raises(SystemExit, main, "-h --verbose".split())
		assert_raises(SystemExit, main, "-h".split())