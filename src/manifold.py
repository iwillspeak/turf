import numpy as np
import mdp
import math
import operator

METRICS = ['dg', 'dbc', 'dp']

_square = lambda x: x ** 2

class Grassmann(object):
	"""Grassmannian Manifold Object
	
	Represents a grassmannian manifold of image vectors. Each image vector is
	passed to the constructor and then principle component analysis creates 
	the vectors that make up the manifold.
	"""
	
	def __init__(self, *ivectors):
		
		if len(ivectors) < 2:
			raise VectorDimensionError()

		# Check that all the vectors are of the same order
		if any([len(x) != len(ivectors[0]) for x in ivectors]):
			raise VectorDimensionError()
		
		# Prevent anything silly for the moment
		if len(ivectors[0]) > (32 * 32):
			raise VectorDimensionError()

		# Ensure that the input data is in float form
		self.matrix = np.array([np.array(x, dtype=np.float64) for x in ivectors])
		self.pcadata = None
	
	def get_basis(self):
		# Lazy PCA evaluation, This should help keep things snappy
		if self.pcadata == None:
			# Create a pca node to analyse this set of images
			pcanode = mdp.nodes.PCANode(output_dim=5, svd=True)
			
			data = self.matrix
			
			# train the node and extract the subspaces that make up the manifold
			pcanode.train(data)
			pcanode.stop_training()
			
			self.pcadata = np.array(pcanode.v)
			self.matrix = None
		
		return self.pcadata
	
	def _cos_angles_to(self, other):
		
		x = np.asmatrix(self.get_basis())
		y = np.asmatrix(other.get_basis())

		angles = np.linalg.svd(x.transpose() * y, compute_uv=False)

		return [min(abs(a), 1) for a in angles]
		
		
	def _angles_to(self, other):
		
		return map(math.acos, self._cos_angles_to(other))
		
	def _dist_norm(self, dist):
		return 0 if dist < 1.0e-7 else dist
	
	def dg(self, other):
		
		angles = self._angles_to(other)

		return self._dist_norm(math.sqrt(sum(map(_square, angles))))
		
	def dbc(self, other):
		
		angles = self._cos_angles_to(other)
		
		dist = reduce(operator.mul, map(_square, angles))
		
		return self._dist_norm(math.sqrt(1 - dist))
	
	def dp(self, other):
		
		corrs = self._cos_angles_to(other)
		
		m = len(corrs)

		return self._dist_norm(math.sqrt(m - sum(map(_square, corrs))))
		
	def __cmp__(self, other):
		
		# compute a 'sign' this allows the matracies to compare
		# opposite when passed in in reverse order
		alpha = max([max(m) for m in self.get_basis()])
		beta = max([max(m) for m in other.get_basis()])
		sign = -1 if alpha < beta else 1
		
		return sign * self.dg(other)
	
	def __repr__(self):
		return repr(self.get_basis())
	

# Errors for Grassmann
class VectorDimensionError(Exception):
	pass

class ObjectDatabase(dict):
	
	def __init__(self, image_database):
		for k,v in image_database.iteritems():
			self[k] = Grassmann(*v.vectors()), ObjectDatabase(v) if v.is_database() else None
	
	def iterclasses(self):
		return ((k, v[0]) for k, v in self.iteritems())
			
	def iterobjects(self):
		for k, v in self.iteritems():
			if v[1]:
				for _, o in v[1].iterobjects():
					yield k, o
			else:
				yield k, v[0]
