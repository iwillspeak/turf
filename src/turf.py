#! /usr/bin/env python
import sys
import os
import manifold
import images
import classifier
import pickle
from argparse import ArgumentParser

def _cache_is_viable(cache_path, db_path):
	try:
		return os.path.getmtime(cache_path) > os.path.getmtime(db_path)
	except os.error:
		# We don't have one of the files in question, so give up now
		return False
	
def load_database(db_path, parser):
	cache_path = db_path + ".cdb"
	db = None

	# Attempt to load the database from the cahce file first
	try:
		if _cache_is_viable(cache_path, db_path):
			with open(cache_path, "r") as f:
				db = pickle.load(f)
	except pickle.UnpicklingError, IOError:
		print "Failed to load cache, rebuilding database"
	
	# if we managed to load the datbase then return it now
	if db:
		return db
	
	# Create the database if we didn't load it from cache
	try:
		im_database = images.ImageDatabase(db_path, basepath=os.path.dirname(db_path))
		db =  manifold.ObjectDatabase(im_database)
	except EnvironmentError as e:
		parser.error("Error loading image database file '{0}'".format(e.filename))
	except manifold.VectorDimensionError:
		parser.error("Image manifold vector dimension error")

	# write the database to the cache file
	with open(cache_path, "w") as f:
		pickle.dump(db, f)
	
	return db

def main(argv):
	
	# Simple command line arguments. List of images to be constructed into 
	# a grassmannian and matched and a file to use for the databsae. If none 
	# is given then a default value will be used
	parser = ArgumentParser(description = "Turf - Object Recogniser",
	                        epilog = 'For more information read the source code...')
	parser.add_argument('-d', '--database', metavar='DATABSE', default="database.yaml",
	                    help = "Database file. This is a YAML document specifying the training set to use")
	parser.add_argument('-n', '--neighbours', default=3, type=int,
	                    help = "Number of neighbours to take into account when classifying.")
	parser.add_argument('image', help = "Image file to recognise", nargs="+")
	parser.add_argument('-m', '--metric', default="dg", type=str,
	                    help = "Distance metric to use, on of dg, dbc, and dp")
	parser.add_argument('-v', '--verbose', help="Be verbose during the run",
						action="store_true")
	options = parser.parse_args(argv)
	
	metrics = manifold.METRICS
	if not options.metric in metrics:
		print "Distance metric must be one of '{0}'".format("' '".join(metrics))
		print "Falling back to '{0}'".format(metrics[0])
		parser.print_usage()
		options.metric = metrics[0]
	
	if options.verbose: print "Loading database..."
	database = load_database(options.database, parser)
	
	if options.verbose: print "Loading test image..."
	sample_image_vectors = images.ImageSet(options.image).vectors()
	sample_manifold = manifold.Grassmann(*sample_image_vectors)
	
	if options.verbose: print "Creating classifier..."
	clas = classifier.Classifier(database, options.neighbours, options.metric)
	
	if options.verbose: print "Classifying..."
	print "Object looks like a", clas(sample_manifold)
	if options.verbose:
		print "(considered neighbourhood {0})".format(clas.neighbourhood)
	
	# Re-save the cache, in case we performed more PCA this time
	with open(options.database + ".cdb", "w") as f:
		pickle.dump(database, f)

if __name__ == "__main__":
	sys.exit(main(sys.argv[1:]))
