import Image, ImageOps
import numpy as np
import os
import yaml
import operator

IMSIZE = 32

class ImageSet(object):
	
	def __init__(self, *images):
		"""Create an ImageSet
		
		Accepts either a single list of images or one or more images as 
		parameters. These can either be paths to image files or open file 
		objects.
		"""
		if len(images) == 1 and type(images[0]) == list:
			self.images = images[0]
		else:
			self.images = images
	
	def vectors(self):
		for f in self.images:
			try:
				f.seek(0, os.SEEK_SET)
			except AttributeError:
				pass
			
		return map(lambda i: self._vector_for_image(Image.open(i)), self.images)
	
	def _vector_for_image(self, image):
		size = (IMSIZE, IMSIZE)
		image = ImageOps.fit(image, size, Image.ANTIALIAS)
		a = np.asarray(ImageOps.autocontrast(image.convert("L")))
		return a.reshape(1, -1)[0]
		
	def is_database(self):
		return False
	

class ImageDatabase(dict):
	
	def __init__(self, database, basepath=None):
		if isinstance(database, dict):
			self.y = database
		else:
			try:
				with open(database) as d:
					self.y = yaml.load(d)
			except IOError:
				self.y = yaml.load(database)
		
		self.basepath = basepath if basepath else os.curdir
		
		if not self.y:
			return
		
		for otype, val in self.y.iteritems():
			if isinstance(val, dict):
				self[otype] = ImageDatabase(val, self.basepath)
			else:
				self[otype] = ImageSet(self._images_for_places(val, self.basepath))
	
	def is_database(self):
		return True
	
	def sets(self):
		for i in self.values():
			if isinstance(i, ImageDatabase):
				for s in i.sets():
					yield s
			else:
				yield i
	
	def itersets(self):
		for k, i in self.iteritems():
			if isinstance(i, ImageDatabase):
				for s in i.sets():
					yield k, s
			else:
				yield k, i
	
	def vectors(self):
		return reduce(operator.add, map(lambda x: x.vectors(), self.values()))
		
	def _pngs_in_dir(self, directory):
		return [os.path.join(directory, l) for l in os.listdir(directory) if l.endswith('.png')]
	
	def _images_for_places(self, places, basepath):
		ivectors = []
		for p in places:
			location = os.path.join(basepath, p)

			if os.path.isdir(location):
				paths = self._pngs_in_dir(location)
			else:
				paths = [location]

			ivectors.extend(paths)
		return ivectors
