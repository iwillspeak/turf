
class Classifier(object):
	
	def __init__(self, database, neighbours=3, metric="dg"):
		if not len(database):
			raise EmptyDatabaseError

		self.database = database
		self.neighbours = neighbours
		self.metric = metric
		self.neighbourhood = None

	def __call__(self, object):
		return self.execute(object)
		
	def get_sorter(self, test_object):
		"""Sorting Method Generator
		
		This method is called to generate a method that will
		be used to sort the matches from the database. Subclasses
		can override it to provide differnet classifiers 
		based upon different distance measurements. The function
		should accept a single value, a tuple of object_type and
		manifold and return a key specifying how good the match
		is. The lower the value the better.
		"""
		
		def sorter(stuff):
			objtype, manifold = stuff
			return getattr(manifold, self.metric)(test_object)
		return sorter
	
	def execute(self, test_object):
		"""Perform the Classification
		
		Returns a string from the database passed into
		the consttructor of the classifier that most
		closely describes `test_object`. If there are
		several objects near then `self.neighbours` are
		taken into account in the classification.
		"""
		
		things = self.database.iterobjects()
		
		guesses = [k for k, _ in sorted(things, key=self.get_sorter(test_object))][:self.neighbours]

		# Store the neighbourhood for later
		self.neighbourhood = guesses

		# Create a mapping to hold the current frequency of the matches
		freqs = dict.fromkeys(guesses, 0)
		leader = guesses[0]
		
		for g in guesses:
			# Increment the frequency of the values
			freqs[g] += 1
			
			# If we have a better candidate then use that instead. We use
			# strict greater than to bias towards the first of the matches
			# as this was geodesically closer.
			if freqs[g] > freqs[leader]:
				leader = g
			
		return leader
		
# Error if there are no objects in the datbase to classify
class EmptyDatabaseError(Exception):
	pass