from manifold import *
from nose.tools import assert_raises
import numpy as np
import itertools

class TestGrassmann(object):
	
	def test_create(self):
		
		assert Grassmann([2, 34, 45], [1234, 21312, 123])
		assert Grassmann([2, 21, 124, 24, 3], [124, 123, 123, 321, 2])
		assert Grassmann([2, 34, 45], [1234, 21312, 123], [1, 2, 3])
		assert Grassmann([2, 21, 124, 24, 3], [124, 123, 123, 321, 2], [1, 23, 4, 5, 3])
		
		assert Grassmann(np.array([2, 34, 45], dtype=np.uint8),
		                 np.array([1234, 21312, 123], dtype=np.uint8))
		assert Grassmann(np.array([2, 21, 124, 24, 3], dtype=np.uint8),
		                 np.array([124, 123, 123, 321, 2], dtype=np.uint8))
		assert Grassmann(np.array([2, 34, 45], dtype=np.uint8),
		                 np.array([1234, 21312, 123], dtype=np.uint8),
		                 np.array([1, 2, 3], dtype=np.uint8))
		assert Grassmann(np.array([2, 21, 124, 24, 3], dtype=np.uint8),
		                 np.array([124, 123, 123, 321, 2], dtype=np.uint8),
		                 np.array([1, 23, 4, 5, 3], dtype=np.uint8))
	
	def test_invalid_create(self):
		assert_raises(VectorDimensionError, 
		              lambda : Grassmann())
		
		assert_raises(VectorDimensionError, 
		              lambda : Grassmann([12, 123, 312]))
		
		assert_raises(VectorDimensionError,
		              lambda : Grassmann([12], []))
		
		assert_raises(VectorDimensionError,
		              lambda : Grassmann([12, 12], [12]))
		
		assert_raises(VectorDimensionError,
		              lambda : Grassmann([12, 12], [12, 12, 234]))
	
	def test_pca(self):
		
		self._check_pca_array(
			[
				[32, 2, 3],
				[ 3, 4, 6]
			],
			np.array([
				[-0.99781209,  0.06611378],
				[ 0.06611378,  0.99781209]
			])
		)
		
		self._check_pca_array(
			[
				[  0,   0, 255,   0,   0],
				[  0, 255, 255, 255,   0],
				[255, 255, 255, 255, 255],
				[  0, 255, 255, 255,   0],
				[  0,   0, 255,   0,   0]
			],
			np.array([
				[ -3.71748034e-01,   6.01500955e-01,  -6.66500359e-01,
				  -2.36172121e-01],
				[ -6.01500955e-01,  -3.71748034e-01,  -2.36172121e-01,
				   6.66500359e-01],
				[  0.00000000e+00,   0.00000000e+00,   1.11022302e-16,
				  -1.66533454e-16],
				[ -6.01500955e-01,  -3.71748034e-01,   2.36172121e-01,
				  -6.66500359e-01],
				[ -3.71748034e-01,   6.01500955e-01,   6.66500359e-01,
				   2.36172121e-01]
			])
		)
	
	def _check_pca_array(self, matrix, pca_matrix):
		
		m = Grassmann(*matrix)
		
		raw_pca = m.get_basis()
		
		# make sure we're dealing with orthogonal matrixes
		test = lambda x: np.allclose(x.transpose() * x, np.identity(x.shape[1]))
		map(test, map(np.asmatrix, (raw_pca, pca_matrix)))
		
		# Make sure that all vectors are unit length
		lengs = map(sum, map(lambda x: x * x, raw_pca.transpose()))
		assert np.allclose(lengs, np.ones(len(lengs)))

		# compare the actual data, with a tolerence
		# TODO: remove this fudge somehow, either get rid of the test or 
		# update the cooked data
		#assert raw_pca.shape == pca_matrix.shape
		#assert np.allclose(raw_pca, pca_matrix)
	
	def _get_manifolds(self):
		
		a = Grassmann(
		[113, 211, 106, 155, 156, 183, 249, 218, 141, 186, 107, 225, 68, 50, 88, 241, 55, 135, 101, 144, 243, 217, 195, 77, 131, 254, 66, 164, 216, 240, 42, 202],
		[214, 158, 16, 75, 21, 29, 128, 242, 91, 15, 202, 59, 240, 109, 143, 139, 58, 137, 253, 76, 217, 219, 249, 41, 152, 35, 145, 43, 188, 88, 106, 117],
		[124, 54, 116, 190, 215, 46, 182, 55, 126, 62, 14, 2, 136, 6, 157, 217, 79, 94, 103, 245, 10, 223, 156, 45, 15, 85, 110, 101, 32, 149, 129, 27],
		[157, 175, 20, 12, 208, 89, 200, 198, 83, 69, 168, 189, 161, 241, 238, 142, 246, 162, 88, 236, 184, 26, 220, 252, 204, 42, 115, 62, 147, 153, 23, 227])
		b = Grassmann(
		[34, 106, 251, 159, 231, 108, 161, 6, 29, 131, 59, 229, 221, 170, 8, 208, 180, 7, 133, 23, 235, 118, 151, 216, 202, 247, 67, 44, 89, 60, 86, 146],
		[85, 82, 71, 55, 46, 113, 86, 100, 212, 74, 83, 172, 160, 93, 231, 23, 29, 142, 248, 249, 116, 198, 57, 53, 88, 118, 235, 146, 161, 103, 5, 25],
		[127, 136, 1, 190, 242, 68, 148, 30, 188, 180, 101, 65, 49, 66, 167, 4, 160, 216, 184, 48, 199, 130, 54, 73, 162, 57, 225, 70, 152, 166, 51, 217],
		[2, 63, 139, 16, 182, 33, 144, 248, 88, 80, 48, 145, 184, 133, 76, 172, 217, 221, 246, 204, 106, 23, 216, 84, 250, 229, 0, 99, 165, 64, 224, 193])
		c = Grassmann(
		[73, 98, 83, 142, 39, 177, 244, 113, 132, 147, 125, 206, 51, 194, 130, 100, 41, 49, 221, 249, 149, 52, 9, 2, 1, 159, 25, 106, 237, 65, 58, 55],
		[219, 249, 193, 129, 110, 27, 4, 223, 171, 250, 167, 116, 5, 242, 213, 205, 142, 147, 102, 225, 136, 13, 25, 217, 52, 137, 93, 178, 235, 188, 144, 72],
		[96, 167, 108, 20, 191, 97, 70, 213, 125, 6, 154, 187, 30, 15, 220, 177, 127, 146, 89, 170, 104, 106, 112, 76, 59, 183, 129, 114, 172, 137, 62, 8],
		[243, 232, 55, 218, 101, 251, 111, 4, 126, 208, 189, 57, 221, 250, 186, 249, 79, 22, 238, 202, 91, 209, 99, 162, 28, 67, 159, 32, 85, 93, 90, 123])
		d = Grassmann(
		[126, 73, 59, 204, 35, 2, 133, 3, 92, 84, 239, 30, 17, 174, 143, 117, 234, 54, 44, 225, 203, 191, 171, 221, 199, 167, 120, 1, 168, 209, 95, 70],
		[146, 204, 145, 126, 138, 1, 39, 153, 239, 106, 79, 225, 237, 47, 86, 158, 131, 179, 92, 112, 143, 234, 157, 116, 68, 191, 58, 44, 26, 90, 150, 217],
		[70, 45, 83, 211, 241, 237, 117, 225, 203, 238, 119, 195, 187, 88, 44, 115, 66, 39, 176, 168, 17, 220, 233, 0, 108, 65, 20, 213, 230, 102, 134, 234],
		[161, 209, 171, 221, 50, 208, 117, 65, 140, 232, 13, 157, 68, 158, 187, 86, 166, 33, 225, 214, 22, 177, 146, 222, 85, 172, 143, 124, 53, 200, 165, 98])
		e = Grassmann(
		[138, 136, 10, 81, 189, 40, 36, 79, 161, 240, 54, 46, 26, 251, 78, 116, 216, 196, 241, 52, 154, 125, 188, 194, 222, 157, 238, 80, 74, 199, 201, 130],
		[5, 96, 204, 29, 16, 172, 213, 216, 210, 41, 64, 147, 237, 136, 120, 23, 157, 61, 178, 84, 95, 39, 56, 191, 104, 224, 73, 22, 82, 109, 127, 99],
		[109, 52, 187, 231, 173, 33, 100, 221, 216, 176, 64, 53, 156, 119, 219, 130, 244, 68, 253, 144, 125, 224, 150, 66, 172, 41, 32, 29, 157, 49, 254, 122],
		[198, 21, 160, 32, 89, 151, 189, 102, 197, 3, 29, 117, 204, 61, 195, 171, 123, 17, 131, 0, 243, 218, 54, 49, 30, 165, 104, 175, 33, 208, 232, 222])
		
		return [a, b, c, d, e]
	
	def test_compare(self):
		
		arr = self._get_manifolds()

		# some simple comparison truthes
		for x in arr:
			assert x == x
		for x, y in itertools.permutations(arr, 2):
			assert x != y
			assert (x < y) or (y < x)
			assert (x < y) != (y < x)
			assert (x < y) == (y > x)
			
		# all permutations of an array should be the same when sorted
		sarr = sorted(arr)
		
		for x in itertools.permutations(arr):
			# not only do we test the comparison operator in sorting
			# we also check it in this equality check
			assert sorted(x) == sarr
	
	def test_angle(self):
		arr = self._get_manifolds()
		
		for g, o in itertools.permutations(arr, 2):
			angs = g._angles_to(o)
			assert angs != None
			assert len(angs)
			for a in angs:
				assert a > 0
				assert a <= (math.pi / 2)
	
	def _test_dist_with_method(self, methodname):
		
		arr = self._get_manifolds()
		
		# A manifold should be 0 units from itself
		for a in arr:
			# The distance metric should normalise small distances to 0.0 so
			# test directly for equality
			assert getattr(a, methodname)(a) == 0.0
		
		# There is a distance between two distinct manifodls
		# and it is communtative
		for a, b in itertools.permutations(arr, 2):
			assert getattr(a, methodname)(b) > 0
			assert (getattr(a, methodname)(b) - getattr(b, methodname)(a)) < 1.0e-8
		
		# The distance between two manifolds is further via a 
		# third manifold
		for a, b, c in itertools.permutations(arr, 3):
			assert getattr(a, methodname)(b) + getattr(b, methodname)(c) >= getattr(a, methodname)(c)
		
	def test_geodesic_dist(self):
		
		self._test_dist_with_method("dg")
	
	def test_binet_cauchy(self):
		
		self._test_dist_with_method("dbc")
		
	def test_projecttion_dist(self):
		
		self._test_dist_with_method("dp")

class DummyImageSet(object):
	
	def __init__(self, vecs):
		self.vecs = vecs
	
	def vectors(self):
		return self.vecs
	
	def is_database(self):
		return False

class DummyImageDatabase(object):
	
	def __init__(self, mapping):
		self.mapping = mapping
	
	def sets(self):
		return True
	
	def vectors(self):
		return reduce(operator.add, [i.vectors() for i in self.mapping.values()])
		
	def iteritems(self):
		return self.mapping.iteritems()
		
	def is_database(self):
		return True

class TestObjectDatabase(object):
	
	def test_create(self):
		vec1 = [1,  2, 3,  4]
		vec2 = [21, 2, 3, 89]
		vec3 = [12, 3, 4, 56]

		d1 = ObjectDatabase({"Fudge": DummyImageSet([vec1, vec2])})
		d2 = ObjectDatabase({"Apple": DummyImageSet([vec1, vec2, vec1])})
		
		assert_raises(VectorDimensionError, lambda : ObjectDatabase({"k": DummyImageSet([vec1])}))
		
		assert d1
		assert d2
		assert isinstance(d1["Fudge"][0], Grassmann)
		assert not d1["Fudge"][1]
		assert isinstance(d2["Apple"][0], Grassmann)
		assert not d2["Apple"][1]
		assert_raises(KeyError, lambda : d2["invalidl"])
		assert_raises(KeyError, lambda : d1["Not Here"])
		
		d3 = ObjectDatabase({"A": DummyImageSet([vec1, vec2]), "B": DummyImageDatabase({"B1": DummyImageSet([vec2, vec3]), "B2": DummyImageSet([vec3, vec1])})})
		assert d3
		assert isinstance(d3["A"][0], Grassmann)
		assert not d3["A"][1]
		assert isinstance(d3["B"][0], Grassmann)
		assert isinstance(d3["B"][1], ObjectDatabase)
		d4 = d3["B"][1]
		assert isinstance(d4["B1"][0], Grassmann)
		assert not d4["B1"][1]
		assert isinstance(d4["B2"][0], Grassmann)
		assert not d4["B2"][1]
	
	def test_iteration(self):
		vec1 = [1,  2, 3,  4]
		vec2 = [21, 2, 3, 89]
		vec3 = [12, 3, 4, 56]
		
		d3 = ObjectDatabase({"A": DummyImageSet([vec1, vec2]), "B": DummyImageDatabase({"B1": DummyImageSet([vec2, vec3]), "B2": DummyImageSet([vec3, vec1])})})
		
		assert d3.iterobjects()
		assert d3.iterclasses()
		
		assert len(list(d3.iterclasses())) == 2
		assert len(list(d3.iterobjects())) == 3