from images import *
from tempfile import NamedTemporaryFile
from nose.tools import assert_raises
import Image
import numpy as np

class VectorChecker(object):

	def _check_vector(self, vector, leng):
		
		assert len(vector) == leng
		assert all((isinstance(i, np.ndarray) for i in vector))
		assert all(len(i) == (IMSIZE * IMSIZE) for i in vector)

	def _dummy_image_file(self, imagedata, size):
		t = NamedTemporaryFile(suffix=".png")
		tempimage = Image.new("RGB", size)
		tempimage.putdata(imagedata)
		tempimage.save(t)
		t.seek(0, os.SEEK_SET)
		return t
		
	def _temp_images(self, images):
		return [self._dummy_image_file(*im) for im in images]
	
class TestImageSet(VectorChecker):

	def test_create(self):
		assert ImageSet(["Hello"])
		assert ImageSet(["Hello", "World"])
		assert ImageSet("Hello", "World")
		
		assert not ImageSet("Hello", "World").is_database()

	def _check_image_set(self, *images):

		tempimages = self._temp_images(images)

		# check both use cases for the function, also checks that all files
 		# are rewownd before use
		u = ImageSet(tempimages).vectors()
		v = ImageSet(*tempimages).vectors()
		w = ImageSet(*[f.name for f in tempimages]).vectors()
		x = ImageSet(*([f.name for f in tempimages] + tempimages)).vectors()

		self._check_vector(u, len(images))
		self._check_vector(v, len(images))
		self._check_vector(w, len(images))
		self._check_vector(x, len(images) * 2)

	def test_vector(self):
		imdata = [(255, 0, 0), (123, 0, 0),
		          (255, 0, 0), (123, 0, 0)]

		self._check_image_set((imdata, (2,2)))
		self._check_image_set((imdata, (2,2)), (imdata, (2,2)))
		self._check_image_set((imdata, (2,2)), (imdata, (2,2)), (imdata, (2,2)))
	

class TestImageDatabase(VectorChecker):
	
	db1str = """
inline_things:
 - /path/one
 - /path/two
"""

	def test_create(self):
		
		# Pretty much only one valid use case for create
		with NamedTemporaryFile(suffix=".yaml") as f:
			d = ImageDatabase(f.name)
		with NamedTemporaryFile(suffix=".yaml") as f:
			f.write(self.db1str)
			f.seek(0, os.SEEK_SET)
			assert ImageDatabase(f.name)
		assert ImageDatabase(self.db1str)
		
		# Check creation of mapping (unpacked) database
		imdata = [(255, 0, 0), (123, 0, 0),
		          (255, 0, 0), (123, 0, 0)]
		images = [(imdata, (2,2)), (imdata, (2,2)), (imdata, (2,2))]
		db = ImageDatabase({"A" : [f.name for f in self._temp_images(images)]})
		assert db != None
		
		assert db.is_database()
		
		# chek the invalid use cases
		assert_raises(TypeError, lambda : ImageDatabase())

	def test_iteration(self):
		db = ImageDatabase(self.db1str)
		assert db.iteritems()
		assert all((len(x) == 2 for x in db.iteritems()))
		for x in db.iteritems():
			getattr(x[1], "vectors")
	
	def test_paths(self):
		db1 = ImageDatabase(self.db1str)
		assert len(db1) == 1
		assert len(db1) == len([k for k, v in db1.iteritems()])
		for (key, imset), name in zip(db1.iteritems(), ["inline_things"]):
			assert key == name
		for (key, imset), name in zip(db1.iteritems(), ["inline_things"]):
			assert key == name
	
	def test_vectorise(self):
		imdata = [(255, 0, 0), (123, 0, 0),
		          (255, 0, 0), (123, 0, 0)]
		images = self._temp_images([(imdata, (2,2)), (imdata, (2,2)), (imdata, (2,2))])
		db = ImageDatabase({"A" : [f.name for f in images]})
		
		assert db.vectors()
		assert len(db.vectors()) > 1
		self._check_vector(db.vectors(), 3)
	
	def test_sets(self):
		imdata = [(255, 0, 0), (123, 0, 0),
		          (255, 0, 0), (123, 0, 0)]
		images = self._temp_images([(imdata, (2,2)), (imdata, (2,2)), (imdata, (2,2))])
		names = [f.name for f in images]
		db = ImageDatabase({"A" : names, "B" : names})
		
		assert db.sets()
		assert len(list(db.sets())) == 2
		
		db = ImageDatabase({"TOP1": {"A": names, "B": names}, "TOP2": names})
		assert db.sets()
		assert len(list(db.sets())) == 3
		assert sorted(db.keys()) == sorted(["TOP1", "TOP2"])
		
	def test_setiter(self):
		imdata = [(255, 0, 0), (123, 0, 0),
		          (255, 0, 0), (123, 0, 0)]
		images = self._temp_images([(imdata, (2,2)), (imdata, (2,2)), (imdata, (2,2))])
		names = [f.name for f in images]
		
		db = ImageDatabase({"TOP1": {"A": names, "B": names}, "TOP2": names})
		assert db.itersets()
		assert len(list(db.itersets())) == 3
		assert sorted([k for k, v in db.itersets()]) == ["TOP1", "TOP1", "TOP2"]
