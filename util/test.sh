#! /usr/bin/env bash

if command -v snooper 1>&2 > /dev/null
then
	snooper
else
	
	pushd src
	
	if command -v nosy 1>&2 > /dev/null
	then
		nosy
	else
		while true
		do
			nosetests
			sleep 10
		done
	fi
fi