#! /usr/bin/env bash

db=Images/database.yaml
folder=Images/eth80-cropped256/

echo "Apple"
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}apple10/*.png > /dev/null 2>&1
echo "Car"   
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}car14/*.png > /dev/null 2>&1
echo "Cow"   
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}cow10/*.png > /dev/null 2>&1
echo "Cup"   
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}cup10/*.png > /dev/null 2>&1
echo "Dog"   
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}dog10/*.png > /dev/null 2>&1
echo "Horse" 
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}horse10/*.png > /dev/null 2>&1
echo "Pear"  
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}pear10/*.png > /dev/null 2>&1
echo "Tomato"
rm "${db}.cdb"
time ./src/turf.py -n 5 -d $db ${folder}tomato10/*.png > /dev/null 2>&1