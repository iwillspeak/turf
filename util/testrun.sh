#! /usr/bin/env bash

db=Images/database.yaml
folder=Images/eth80-cropped256/

echo "Apple"
./src/turf.py -n 5 -d $db ${folder}apple10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Car"   
./src/turf.py -n 5 -d $db ${folder}car14/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Cow"   
./src/turf.py -n 5 -d $db ${folder}cow10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Cup"   
./src/turf.py -n 5 -d $db ${folder}cup10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Dog"   
./src/turf.py -n 5 -d $db ${folder}dog10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Horse" 
./src/turf.py -n 5 -d $db ${folder}horse10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Pear"  
./src/turf.py -n 5 -d $db ${folder}pear10/*.png | grep "looks like a" | cut -f 5 -d " "
echo "Tomato"
./src/turf.py -n 5 -d $db ${folder}tomato10/*.png | grep "looks like a" | cut -f 5 -d " "