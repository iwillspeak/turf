#! /usr/bin/env python
import os
import sys

def process_folder(folder):
    files, tests = [], []
    
    for f in os.listdir(folder):
        if not f.endswith(".py"):
            continue
        s = r"\lstinputlisting[name={0}]{{{1}{0}}}".format(f, folder)
        
        if f.startswith("test"):
            tests.append(s)
        else:
            files.append(s)
        
    return files,tests
    
def print_with_title(title, listings):
    print r"\chapter{" + title + "}"

    for l in listings:
        print l

if __name__ == "__main__":
    files, tests = [], []
    
    for folder in sys.argv[1:]:
        
        f,t = process_folder(folder)
        files.extend(f)
        tests.extend(t)
    
    print_with_title("Source Listings", files)
    print_with_title("Unit Test Listings", tests)    