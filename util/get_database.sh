#! /usr/bin/env bash

pushd Images

echo "Downloading..."
url="http://www.d2.mpi-inf.mpg.de/sites/default/files/datasets/eth80/eth80-cropped256.tgz"
if command -v curl 1>&2 > /dev/null
then
	curl -O ${url}
else
	wget ${url}
fi

echo "Unpacking..."
tar -zxf eth80-cropped256.tgz

popd
