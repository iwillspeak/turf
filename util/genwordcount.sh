#! /bin/sh

if [ $# -ne 1 ]
then
	echo "Useage: genwordcount <filename>"
	exit 1
fi

ps2ascii "$1" | wc -w | xargs echo
