Object Recognition using Grassmannian Manifolds
===============================================

This  is  the source  code  for  my final  year  project.  It consists  of  an
application that performs object recognition  from a database of known objects
using Grassmannian Manifolds.  Each object is represented by a  set of images.
The data  from these  images is  embedded into a  higher dimensional  space by
considering  it as  the  span of  a  manifold. Objects  are  then compared  by
calculating  the distance  between the  manifolds based  upon their  principle
angles.

Prerequisites
-------------

 * The Python Imaging Library
 * Python 2.6 or greater
 * NumPy or SciPy
 * MDP

Usage
-----

The command line executable, `turf.py`, is located in the `src` folder and has
a         few         simple          command         line         parameters:

    usage: turf.py [-h] [-d DATABSE] [-n NEIGHBOURS] [-m METRIC] image [image ...]
    
    Turf - Object Recogniser
    
    positional arguments:
      image                 Image file to recognise
    
    optional arguments:
      -h, --help            show this help message and exit
      -d DATABSE, --database DATABSE
                            Database file. This is a YAML document specifying the
                            training set to use
      -n NEIGHBOURS, --neighbours NEIGHBOURS
                            Number of neighbours to take into account when
                            classifying.
      -m METRIC, --metric METRIC
                            Distance metric to use, on of dg, dbc, and dp
    
    For more information read the source code...
